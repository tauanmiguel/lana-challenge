package store

type Product struct {
	Code, Name string
	Price      int64
	Quantity   int64
}

func (p *Product) Value() (v int64) {
	v = p.Price * p.Quantity
	return v
}

package store

import (
	"html/template"
)

type Store struct {
	Destroy           chan string
	NewBasket         chan Basket
	AvailableProducts []Product
	Baskets           []Basket
	Tpl               *template.Template
}

func (s *Store) GetBasketIndex(id string) int {
	i := SliceIndex(len(s.Baskets), func(i int) bool { return s.Baskets[i].Id == id })
	return i
}

func (s *Store) AddBasket(b *Basket) {
	s.Baskets = append(s.Baskets, *b)
}

func (s *Store) DestroyBasket(id string) {
	i := s.GetBasketIndex(id)
	copy(s.Baskets[i:], s.Baskets[i+1:])
	s.Baskets[len(s.Baskets)-1] = Basket{"nil", make([]Product, len(s.AvailableProducts))} // or the zero value of T
	s.Baskets = s.Baskets[:len(s.Baskets)-1]
}

func (s *Store) Listen() {
	go func() {
		for {
			select {
			case m := <-s.NewBasket:
				s.AddBasket(&m)
			case m := <-s.Destroy:
				s.DestroyBasket(m)
			}
		}
	}()
}

func Max(x, y int64) int64 {
	if x < y {
		return y
	}
	return x
}

func SliceIndex(limit int, predicate func(i int) bool) int {
	for i := 0; i < limit; i++ {
		if predicate(i) {
			return i
		}
	}
	return -1
}

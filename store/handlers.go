package store

import (
	"github.com/julienschmidt/httprouter"
	"github.com/satori/go.uuid"
	"net/http"
)

func (s *Store) HandleHome(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	s.Tpl.ExecuteTemplate(w, "index.gohtml", s.Baskets)
}

func (s *Store) HandleCreateBasket(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	id, _ := uuid.NewV4()
	p := make([]Product, len(s.AvailableProducts))
	copy(p, s.AvailableProducts)
	b := Basket{
		id.String(),
		p,
	}
	s.NewBasket <- b
	http.Redirect(w, r, "/basket/"+id.String(), http.StatusSeeOther)
}

func (s *Store) HandleBasket(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")
	index := s.GetBasketIndex(id)
	s.Tpl.ExecuteTemplate(w, "basket.gohtml", s.Baskets[index])
}

func (s *Store) HandleAddProduct(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")
	r.ParseForm()
	product := r.Form.Get("product")
	index := s.GetBasketIndex(id)
	s.Baskets[index].AddProduct(product)
	s.Tpl.ExecuteTemplate(w, "basket.gohtml", s.Baskets[index])
}

func (s *Store) HandleCheckout(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")
	index := s.GetBasketIndex(id)
	total := s.Baskets[index].CalculateTotal()

	data := struct {
		Total int64
		Id    string
	}{
		total,
		id,
	}
	s.Tpl.ExecuteTemplate(w, "checkout.gohtml", data)
}

func (s *Store) HandleDestroy(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")
	s.Destroy <- id
	s.Tpl.ExecuteTemplate(w, "finish.gohtml", nil)
}

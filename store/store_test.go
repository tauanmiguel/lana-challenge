package store

import (
	"fmt"
	"strconv"
	"testing"
)

func TestVoucherTShirtMug(t *testing.T) {
	// Arrange
	ps := make([]Product, 3)
	ps = append(ps, Product{
		"VOUCHER",
		"Cabify Voucher",
		500,
		1,
	})
	ps = append(ps, Product{
		"TSHIRT",
		"Cabify T-Shirt",
		2000,
		1})
	ps = append(ps, Product{
		"MUG",
		"Cabify Coffee Mug",
		750,
		1,
	})
	b := &Basket{
		"testId",
		ps,
	}
	// Act
	total := b.CalculateTotal()
	// Assert
	if b.CalculateTotal() != 3250 {
		t.Error("Total for Voucher, T-shirt, Mug is " + strconv.FormatInt(total, 10) + " instead of 32,50")
	}
}

func TestVoucherTshirtVoucher(t *testing.T) {
	// Arrange
	ps := make([]Product, 3)
	ps = append(ps, Product{
		"VOUCHER",
		"Cabify Voucher",
		500,
		2,
	})
	ps = append(ps, Product{
		"TSHIRT",
		"Cabify T-Shirt",
		2000,
		1})
	ps = append(ps, Product{
		"MUG",
		"Cabify Coffee Mug",
		750,
		0,
	})
	b := &Basket{
		"testId",
		ps,
	}
	// Act
	total := b.CalculateTotal()
	// Assert
	if total != 2500 {
		fmt.Println(b.CalculateTotal())
		t.Error("Total for two Vouchers and one T-shirt" + strconv.FormatInt(total, 10) + " instead of 81,00")
	}
}

func TestFourTShirtsAndVoucher(t *testing.T) {
	// Arrange
	ps := make([]Product, 3)
	ps = append(ps, Product{
		"VOUCHER",
		"Cabify Voucher",
		500,
		1,
	})
	ps = append(ps, Product{
		"TSHIRT",
		"Cabify T-Shirt",
		2000,
		4})
	ps = append(ps, Product{
		"MUG",
		"Cabify Coffee Mug",
		750,
		0,
	})
	b := &Basket{
		"testId",
		ps,
	}
	// Act
	total := b.CalculateTotal()
	// Assert
	if total != 8100 {
		fmt.Println(b.CalculateTotal())
		t.Error("Total for four T-shirts and a voucher is " + strconv.FormatInt(total, 10) + " instead of 81,00")
	}
}

func TestThreeVoucherThreeTShirtMug(t *testing.T) {
	ps := make([]Product, 3)
	ps = append(ps, Product{
		"VOUCHER",
		"Cabify Voucher",
		500,
		3,
	})
	ps = append(ps, Product{
		"TSHIRT",
		"Cabify T-Shirt",
		2000,
		3})
	ps = append(ps, Product{
		"MUG",
		"Cabify Coffee Mug",
		750,
		1,
	})
	b := &Basket{
		"testId",
		ps,
	}
	total := b.CalculateTotal()
	if total != 7450 {
		t.Error("Total for 3 T-shirts, 3 Vouchers and a mug is " + strconv.FormatInt(total, 10) + " instead of 74,50")
	}
}

package store

import (
	"encoding/csv"
	"os"
	"strconv"
)

const filename string = "products.csv"

func InitProducts() (pl []Product) {
	// Check for errors and open csv
	f, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	// Defer the closing of the file
	defer f.Close()

	// Read through it and check for errors
	lines, err := csv.NewReader(f).ReadAll()
	if err != nil {
		panic(err)
	}
	// Parse through the csv and map the rows into Product Types
	for _, line := range lines[1:] {
		v, err := strconv.ParseInt(line[2], 10, 64)

		if err != nil {
			panic(err)
		}
		pl = append(pl, Product{Code: line[0], Name: line[1], Price: v, Quantity: 0})

	}
	// Return the slice of products
	return pl
}

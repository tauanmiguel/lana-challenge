package store

type Offer interface {
	applyOffer(p *Product)
}

type TwoGetOne struct {
}

type Bulk struct {
}

func (o Bulk) applyOffer(p *Product) {
	if p.Quantity >= 3 {
		p.Price = 1900
	}
}

func (o TwoGetOne) applyOffer(p *Product) {
	var buy int64 = 1
	var get int64 = 1
	r := p.Quantity % (buy + get)
	n := (p.Quantity - r) / (buy + get)
	free := Max(0, r-buy) + (n * get)
	paid := p.Quantity - free
	p.Quantity = paid
}

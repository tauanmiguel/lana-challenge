package store

type Basket struct {
	Id       string
	Products []Product
}

func (b *Basket) CalculateTotal() int64 {
	var total int64 = 0
	for _, p := range b.Products {
		if p.Code == "VOUCHER" {
			o := &TwoGetOne{}
			o.applyOffer(&p)
		}
		if p.Code == "TSHIRT" {
			o := &Bulk{}
			o.applyOffer(&p)
		}
		total += p.Value()
	}

	return total
}

func (b *Basket) AddProduct(p string) {
	// Get index of user's selected product
	index := SliceIndex(len(b.Products), func(i int) bool { return b.Products[i].Code == p })
	// // increment the quantity
	b.Products[index].Quantity++
}

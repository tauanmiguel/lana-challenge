package helpers

import (
	"strconv"
)

func MoneyString(x int64) string {
	i := strconv.FormatInt(x/100, 10)
	n := x % 100

	if n < 10 {
		a := strconv.FormatInt(n, 10)

		return i + "," + "0" + a
	}
	a := strconv.FormatInt(n, 10)

	return i + "," + a
}


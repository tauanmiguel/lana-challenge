package main

import (
	"github.com/julienschmidt/httprouter"
	"github.com/tauanlemos/lana-checkout/helpers"
	"github.com/tauanlemos/lana-checkout/store"
	"html/template"
	"net/http"
)

var tpl *template.Template
var fm = template.FuncMap{
	"ms": helpers.MoneyString,
}

func init() {
	tpl = template.Must(template.New("").Funcs(fm).ParseGlob("templates/*"))
}

func main() {

	// Initialize the products from csv document
	pl := store.InitProducts()
	var bs []store.Basket
	// Make a new Store instance
	s := &store.Store{
		make(chan string),
		make(chan store.Basket),
		pl,
		bs,
		tpl,
	}

	// Listen to actions
	s.Listen()
	router := httprouter.New()
	router.GET("/", s.HandleHome)
	router.POST("/create", s.HandleCreateBasket)
	router.GET("/basket/:id", s.HandleBasket)
	router.POST("/basket/:id/add", s.HandleAddProduct)
	router.GET("/basket/:id/checkout", s.HandleCheckout)
	router.POST("/basket/:id/delete", s.HandleDestroy)

	http.ListenAndServe(":3000", router)
}
